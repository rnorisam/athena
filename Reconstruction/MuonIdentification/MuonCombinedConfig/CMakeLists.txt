################################################################################
# Package: MuonCombinedConfig
################################################################################

# Declare the package name:
atlas_subdir( MuonCombinedConfig )

# Install files from the package:
atlas_install_python_modules( python/*.py  )
# atlas_install_joboptions( share/*.py )
# atlas_install_data( share/*.ref )
