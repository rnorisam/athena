/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack



//
// includes
//

#include <AsgTools/TProperty.h>
#include <AnaAlgorithm/MessageCheck.h>
#include <RootCoreUtils/Assert.h>

//
// method implementations
//

namespace EL
{
  template<typename T> ::StatusCode AnaAlgorithmConfig ::
  setProperty (const std::string& name, const T& value)
  {
    using namespace asg::msgProperty;
    RCU_CHANGE_INVARIANT (this);
    std::string valueString;
    ANA_CHECK (asg::detail::GetCastStringHelper<T>::get (value, valueString));
    m_propertyValues[name] = std::move (valueString);
    return ::StatusCode::SUCCESS;
  }
}
